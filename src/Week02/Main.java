package Week02;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        System.out.println("MILITARY ALPHABET");
        System.out.println();
        System.out.println("--LIST--");
        System.out.println("Returns an array where duplicates can be stored.");
        List list = new ArrayList();
        list.add("Bravo");
        list.add("Zulu");
        list.add("Charlie");
        list.add("Mike");
        list.add("Tango");
        list.add("Mike");

        for (Object str : list) {
            System.out.println((String)str);
        }

        System.out.println();
        System.out.println("Add lines to list.");
        list.add("Bravo Zulu: Good Job");
        list.add("Charlie Mike: Continue Mission");
        list.add("Tango Mike: Thanks Much");

        for (Object str : list) {
            System.out.println((String)str);
        }


        System.out.println();
        System.out.println("--QUEUE--");
        System.out.println("Instead of FIFO of queue the PriorityQueue uses natural order.");
        Queue queue = new PriorityQueue();
        queue.add("Bravo");
        queue.add("Zulu");
        queue.add("Charlie");
        queue.add("Mike");
        queue.add("Tango");
        queue.add("Mike");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println();
        System.out.println("--SET--");
        System.out.println("TreeSet uses natural ordering and doesn't display duplicates.");
        Set set = new TreeSet();
        set.add("Bravo");
        set.add("Zulu");
        set.add("Charlie");
        set.add("Mike");
        set.add("Tango");
        set.add("Mike");

        for (Object str : set) {
            System.out.println((String)str);
        }

        System.out.println();
        System.out.println("--TREEMAP--");
        System.out.println("Stays in Ascending Order");
        Map<Integer, String> map = new TreeMap<>();
        map.put(1,"Bravo");
        map.put(2,"Zulu");
        map.put(3,"Charlie");
        map.put(4,"Mike");
        map.put(5,"Tango");
        map.put(6,"Mike");

        for (Map.Entry<Integer,String> m:map.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }

        System.out.println("Remove 1 & 6");
        map.remove(1);
        map.remove(6);
        for (Map.Entry<Integer,String> m:map.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }

        System.out.println();
        System.out.println("--LIST USING GENERICS--");
        System.out.println("Implement different types of objects at the same time.");
        List<Military> mlist = new LinkedList<Military>();

        mlist.add(new Military("Bravo", "B"));
        mlist.add(new Military("Zulu", "Z"));
        mlist.add(new Military("Charlie", "C"));
        mlist.add(new Military("Mike", "M"));
        mlist.add(new Military("Tango", "T"));

        for (Military military : mlist) {
            System.out.println(military);
        }
    }
}
