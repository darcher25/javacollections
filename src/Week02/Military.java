package Week02;

public class Military {

        private String ma;
        private String meaning;

    public Military(String ma, String meaning) {
            this.ma = ma;
            this.meaning = meaning;
        }

        public String toString() {
            return "Military Alphabet: " + ma + " = " + "Meaning: " + meaning;
        }
}

